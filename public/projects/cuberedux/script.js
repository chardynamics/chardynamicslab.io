function setup() {
	var canvas = createCanvas(400, 400);
	canvas.style('margin', 'auto');
	//canvas.style('border-style', 'none solid solid');
	canvas.parent('script-holder');
	
	//rectMode(CENTER);
	textAlign(CENTER, CENTER);
	textFont(f, 20);
	angleMode(DEGREES);
	noStroke();
  ellipseMode(CENTER);
}
/** Credit:
 * Made with Electric Dolphin's platformer template
 * https://www.khanacademy.org/computer-programming/platformer-template/5484801669693440
 * Used Bob Lyon's Base64 to Image's program
 * https://www.khanacademy.org/computer-programming/base64-png-to-image-converter/4617122376548352
*/
//IF YOU CAN'T GET INTO THE GAME, RELOAD, AND REPEAT
//   _____      _          _____          _      __   __
//  / ____|    | |        |  __ \        | |     \ \ / /
// | |    _   _| |__   ___| |__) |___  __| |_   _ \ V / 
// | |   | | | | '_ \ / _ \  _  // _ \/ _` | | | | > <  
// | |___| |_| | |_) |  __/ | \ \  __/ (_| | |_| |/ . \
//  \_____\__,_|_.__/ \___|_|  \_\___|\__,_|\__,_/_/ \_\

//Changelog
//{

//Update 1.69
//{
/** 
    ___       _____  __ _                                 
   /   |     / ___/ / /(_)____   ____   ___   _____ __  __
  / /| |     \__ \ / // // __ \ / __ \ / _ \ / ___// / / /
 / ___ |    ___/ // // // /_/ // /_/ //  __// /   / /_/ / 
/_/  |_|   /____//_//_// .___// .___/ \___//_/    \__, /  
    ______            /_/    /_/                 /____/   
   / ____/_____ _____ ____ _ ____   ___                   
  / __/  / ___// ___// __ `// __ \ / _ \                  
 / /___ (__  )/ /__ / /_/ // /_/ //  __/                  
/_____//____/ \___/ \__,_// .___/ \___/                   
                         /_/                                         
 * 
*/
//Important
//{
//Added Level 5: A Slippery Escape
//}
//Miscellanous
//{
//Added two minor blocks; spikes underwater and gray background spikes.
//}
//}

//}

//Game Logic
//{
//Scenes:
//{
//Scene 0: Titlescreen/Menu
//Scene 1: The Game
//Scene 2: Credits/"Help" Screen
//Scene 3: Intro
//Scene 4: Leaderboard
//Scene 5: Competitive vs Casual Screen
//Scene 6: Eclipse's Intro
//Scene 7: Competitive Version of the Game
//Scene 8: Other Menu
//}
//}

//Variables
//{
    var win = true;
    var death = 0;
    var acceleration = 0.5;
    var deceleration = 0.4;
    var maxSpeed = 5;
    var jumpHeight = 7.7;
    var gravity = 5;
    var enemySpeed = 1;
    var enemyGravity = 0.09;
    var bounceHeight = 11;
    var enemyBounceHeight = 5;
    var f;


function preload() {
	f = loadFont("/assets/fonts/arialbd.ttf");
}

    var fSize = 20;
    var pulse = 130;
    var pulseS = 1;
    var pulseP = 132;
    var pulseSP = 1;
    var inWater = false;
    var tp = false;
    var scene = 3;
    var angle = 0;
    var rgb = [100,100];
    var fade = 0;
    var time = 0;
    var reduxDebug = true;
    
    {var level = 0;
    var pX = 0;
    var pY = 0;
    var initialize = true;
    var x = 0;
    var y = 0;
    var xVel = 0;
    var yVel = 0;
    var canJump = false;
    var touchingBlock = false;
    var doInit = true;}
    var play = true;
    var competitiveOrNah = false;
    
    var enemies = [];
    
    var playerL = "p";
    var enemyL = "e";
    var blockL = "b";
    var fakeL = "f";
    var lavaL = "l";
    var spikeL = "^";
    var waterSpikeL = "v";
    var graySpikeL = "/";
    var jumpL = "j";
    var portalL = "w";
    var waterL = "o";
    var tp1L = "1";
    var tp2L = "2";
    var tp3L = "3";
    var tp4L = "4";
    var decoRedL = "r";
    var decoGreenL = "g";
    var decoBlueL = "B";
    var wallL = "3";
    var platformL = "4";
    var doorL = "5";
    var waterLavaT = "t";
    var invisiBlockI = "i";
    var decoInvisL = "d";
    //}
    
    //Levels
    //{
    var levels = [
       ["ffffffffffffffffffff",
        "ffffffffffffffffffff",
        "ffffffffffffffffffff",
        "w fffffffff1fffffoff",
        "b  2 ffffffbffff    ",
        "   b    ffffff      ",
        "                    ",
        "                    ",
        "                    ",
        "    o     o     o  j",
        " bbeeeeeeeeeeeeeeebb",
        " bbbbbbbbbbbbbbbbbbb",
        "",
        "",
        "j",
        "bblooolooolooolooob",
        "bbbbbbbbbbbbbbbbbbb",
        "",
        "p                  j",
        "bbbbbbbbbbbbbbbbbbbb"], // Level 1
        
       ["",
        "",
        "",
        "",
        "                   w",
        "        t          b",
        "               tt",
        " bblllllllllllllllbb",
        " bbbbbbbbbbbbbbbbbbb",
        " dddddddddddddddd",
        " dddddddddddddddd",
        " doddddddoddddddd",
        "jddiddiidddddiiid",
        "bblllllllllllllllbb ",
        "bbbbbbbbbbbbbbbbbbb ",
        "",
        "                oo  ",
        "                ^^ j",
        "p  ^  ^^  ^^  ^^bb^b",
        "bbbbbbbbbbbbbbbbbbbb"], // Level 2
        
       ["                   w",
        "2",
        "o",
        "      t",
        "                t",
        " o     ^^^",
        "llllllllllllllllllll",
        "bbbbbbbbbbbbbbbbbbbb",
        "bbbbbbbb",
        "          ",
        "          o",
        " oooooo     ^ tto",
        "1      ^bebeb^     ",
        "bblllllllllllllllbb",
        "bbbbbbbbbbbbbbbbbbb",
        "",
        "  oooooo     ooooo",
        "",
        "p       ^^b^^      j",
        "bbllllllllllllllllbb"], // Level 3
    
       ["llllllllllllllllllll",
        "oooooooooooooooooooo",
        "oooooooooowooooooooo",
        "          l         ",
        "                    ",
        "   ll               ",
        "                    ",
        " o          j       ",
        "                    ",
        "                    ",
        "                    ",
        "          ^bb       ",
        "     b              ",
        "                   o",
        "j                   ",
        "                    ",
        "  l       llll      ",
        "                o   ",
        "p    ^^           j ",
        "bbbbbbbbbbbbbbbbbbbb"], // Level 4
    
        
        ["p",
         "iiiiiiiiiiiiiiiiii  ",
         "lllllllllllllllllloo",
         "oooolooooloooolooooo",
         "oooooooooooooooooooo",
         "oojoooojoooojoooojoo",
         "ooiiiiiiiiiiiiiiiiii",
         "ooooolooooooooollooo",
         "ooooooooollooooooooo",
         "oooooooolllllooooooo",
         "vvvvvvvvvvvvvvvviioo",
         "iiiiiiiiiiiiiiiiiioo",
         "fffffffff2b1ollllloo",
         "ffoffffff2b1oolllloo",
         "ffffffofffbioooooooo",
         "ffff///fffbioooooooo",
         "ffiieeeeeebieeeeeeee",
         "ooiiiiiiiiiiiiiiiiii",
         "ooooooooooooooooooow",
         "iiiiiiiiiiiiiiiiiiii"], // Level 5
        
       ["bbbbbbbbbbbbbbbbbbbb",
        "bfffffffffooooooooow",
        "bffffffffff",
        "bffbfffff",
        "bffffff    b",
        "bffff",
        "bfffff    j   eee",
        "bfbbbbbbbbbbbbbbbbbb",
        "b                  b",
        "bj    bo      bo   b",
        "bbo       bo       b",
        "b                  b",
        "b^^^^^^^^^^^^^^^   b",
        "bbbbbbbbbbbbbbbbbbfb",
        "lllbooooooobfffffffb",
        "   bffffflfffffffffb",
        "   ffffffbfffffffffb",
        "   ffffffbfffffffffb",
        "p  ffblllblfffffffjb",
        "bbbbbbbbbbbbbbbbbbbb"], // level 1
        
       ["pb           w",
        " b           b",
        " b          bf",
        " b         bff",
        " b         f f",
        " b         f f",
        " b     b   f f   ",
        " b     f   f f      ",
        " b     f   f f     j",
        " b   j f   f f     f",
        " bbbbbbf   f f     f",
        " fffffbf  jf f     f",
        " fffffbfffff f     f",
        " bbbffbf   f f     f",
        " b     f   f f j j f",
        " b     f   f fffffff", 
        " b     f   f f f   f",
        " b     f   f f f   f",
        "jbeeeeef   f f f   j",
        "bbbbbbbbbbbbbbbbbbbb"], // level 2
        
       ["ffffffffffffffffffff",
        "ffffffffffffffffffff",
        "fffffffffffoffffffffb",
        "wffffffofffoffffffffb",
        "fffffffffffbffffffffb",
        "ffffffffffffffffffffb",
        "ffffffffffffffffoffb",
        "ffffffffffffffffbffb",
        "ffffffffffffffffofff",
        "ffffffffbfffffffffff",
        "ffffffffofffffffffff",
        "ffffffffffffffffffff",
        "ffffffffffffffffffff",
        "ffffffffffffffffffff",
        "fbbfffffffffffffffff",
        "fbfff   fff   ffffff",
        "fb       f        ff",
        "fb                 f",  
        "pb                 ^",
        "jblllllllllllllllllb"], // level 3
        
       ["oooooooooowooooooooo",
        "",
        "",
        "",
        "",
        "    ^ee^      b",
        "    bbbb   bbb",
        "",
        "2                  ^",
        "jblllllllllllllllllb",
        "bbbbbbbbbbbbbbbbbbbbb",
        "ffffffffffffffffffff",
        "fff fffff  fffff fff",
        "ff   fff    fff   ff", 
        "f  p  f      f     f",
        "f     f      f   1 f",
        "f bbb f  bb  f bbb f",
        "f     f      f     f",
        "ff   fff    fff   ff",
        "lllllllllllllllllllll"], // level 4
        
       ["bbbbbbbbbbbbbbbbbbbb",
        "b233333333333333333b",
        "b33rr3333gg3333BB33b",
        "b33rr3333gg3333BB33b",
        "b33rr3333gg3333BB335bbbbbb",
        "b3333333333333333335eeeeeb",
        "bbbbbbbbbbbbbbbb444bbbbbbb",
        "b                 bb",
        "b                b b",
        "b               b  b",
        "b  bb    bb    b   b",
        "b             b    b",
        "bw^^^^^^^^^^^b^^^^^b",
        "bbbbbbbbbbbbbbbbbbbb",
        "bfffffbffffffbffff1b",
        "b ffffbffffffbfffffb",
        "b  fffbffbbbfbfffffb",
        "b   fffffffbfffffffb",
        "bp   ffffffbfffffffb",
        "bbbbbbbbbllbbbbbbbbb"], // level 5
        
       ["bbbbbbbbbbbbbbbbbbbb",
        "ggggggggggb2BBBBBBBB",
        "ggggggggggbbbbbbbbbB",
        "ggggggggggbBBBBBBBBB",
        "ggggggggggbBBBBBBBBB",
        "ggggggggggbBbbbbBBbb",
        "ggggggggggbBbllbllbb",
        "ggggggggggbBbbbbbbbbbbbbbbbbbb",
        "ggbllllbgg4BBBBBBBBBeeeeeeeeeb",
        "4bbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
        "ffffffffbwb f   f",
        "ffffffffbfb f   f",
        "ffffffffbfbp f f^",
        "ffffffffffbbbb44b",
        "ffffffffffb1bbeeb  b",
        "ffbjffffffb 44bbb",
        "ffffffffffb b  oo",
        "ffffffffbjblbbb",
        "llllllllllbb^^^^^^^^",
        "bbbbbbbbbbbbbbbbbbbb"], // level 6
        
       ["bbbbbbbbbbbbbbbbbbbb",
        "p",
        "bbbbbbbb    bbbbbbbb",
        "llllllll    llllllll",
        "llllllll    llllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "llllll  444lllllllll",
        "llllll  4eelllllllll",
        "llllll  444lllllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "lllllll    lllllllll",
        "llllllll    llllllll",
        "llllllllwwwwllllllll",
        "llllllllllllllllllll",
        "bbbbbbbbbbbbbbbbbbbb"], // level 7
        
       ["",
        "             b",
        "                   o",
        "l  lb",
        "l  lb",
        "l  lb   j",
        "l  lb              b",
        "l  lb",
        "l  lb",
        "l  lb               ",
        "l  lb",
        "l  lb",
        "l  lb        j",
        "l  lb",
        "l  lb",
        "lwwlb    o       4ee",
        "llllb            4bb",
        "bbbbb            4b2",
        "p                4bw",
        "bbbbbbbbbbbbbbbbbbbb"], // level 8
        
       ["bb     lloooollooooo",
        "w    n  llloooooo",
        "          loooooo",
        "          ^bb^^   ^^",
        "   o  bb          bb",
        "   b  o",
        "   b             j",
        "4eeb",
        "444o",
        "ll            o",
        "",
        "   ",
        " bb          o",
        " b",
        " b",
        " b",
        " b",
        "pb",
        "jbllllllllllllllllll",
        "bbbbbbbbbbbbbbbbbbbb"], // level 9
        
        ["bbbbbbbbbbbbbbooooo",
        "                ^^^1",
        "                    ",
        "                    ",
        "                    ",
        "      o       j     ",
        "                    ",
        "         lll        ",
        "                    ",
        "                    ",
        "                    ",
        "      j             ",
        "               o    ",
        "blllllllllllllllbb  ",
        "bbbbbbbbbbbbbbbbbb  ",
        "bbbbbbbbbbbbbbbbbb  ",
        "                   j",
        "            bbbbbbbb",
        "p         eelllllll2",
        "bbbbbbbbbllllbbbbbbw"], //level 10
        
       ["bbbbbbbbbbbbbbbbbbbb",
        "                    ",
        "44444444444444444444",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggrrrggggrrrggggg",
        "gggggrggggggrggggggg",
        "gggggrgrggggrgrggggg",
        "gggggrrrggggrrrggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "gggggggggggggggggggg",
        "44444444444444444444",
        "p                  ",
        "bbbbbbbbbbbbbbbbbbbb"], // celebration screen
    
    ];
    
    
    var levelText = [["Level 1\nTutorial As Hard As A Level\n\n\n\nTeleporters, enemies, more water, you'll\nunderstand it immidately\n\n\n\n\nStay in the water, away from the lava\n\n\nJumpPads help you jump higher", 200, 190], ["Level 2\nTutorial 2: Electric Boogaloo\n\nThis changes between water\nand lava\n\n\n\nWatch out; Fake and real are hard\nto tell\n\n\n\nSpikes are bad for you", 200, 175],["Level 3:\nSmooth Sailing", 200, 30],["Level 4:\n Booby Trapped Rock Wall\nBy OliveVessel156", 200, 40],["Level 5:\nA Slippery Escape", 200, 30],["Finally It Has The Infection", 200, 30],["Level 1", 360, 130],["Level 2\nWait", 200, 30],["Level 3", 200, 10],["Level 4", 200, 30],["Level 5", 200, 30],["Level 6", 100, 40],["Level 7\nDropper\n:P", 70, 150],["Level 8\nParkour!", 200, 200],["Level 9 made by Baljeet", 200, 340],["Thanks for playing", 200, 230]];
    //}
    
    //In-Game AI
    //{
    var player = function() {
        fill(pulse,pulse,pulse);
        if (competitiveOrNah===true) {
            textSize(138);
            text(death,pX+10,pY-10);
        } else {text(death,pX+10,pY-10);}
        if (reduxDebug === false ) {
           // image(steveS, pX, pY, 20, 20);
        } else {
        fill(pulse,pulse,pulse);
        text(death,pX+10,pY-10);
        fill(244, 127, 255);
        rect(pX, pY, 20, 20,5);
        fill(255, 0, 0);
        }
    };
    
    var enemy = function(x, y) {
        this.x = x;
        this.y = y;
        this.dir = round(random(0, 1));
        this.yVel = 0;
    };
    
    enemy.prototype.draw = function() {
        if (reduxDebug === false) {
            //image(slimeS, this.x, this.y, 20, 20);
        } else { fill(100, pulse, pulse); rect(this.x, this.y, 20, 20,5); }
        if (this.dir === 0) {
            this.x -= enemySpeed;
        } else {
            this.x += enemySpeed;
        }
        this.y += this.yVel;
        this.yVel += enemyGravity;
    };
    
    var keys = [];
    
    keyPressed = function() {
        keys[keyCode] = true;
    };
    
    keyReleased = function() {
        keys[keyCode] = false;
    };
    //}
    
    //Leaderboard
    //{
    var leaderboard = function(){
        background(50, pulse, pulse);
        fill(100);
        push();
        stroke(255, 255, 255);
        rect(30,30,340,340,10);
        pop();
        fill(255, 255, 255);
        textSize(45);
        textAlign(CENTER,CENTER);
        text("Leaderboard:",200,70);
        rect(30,110,340,5,10);
        textSize(20);
        textAlign(LEFT, LEFT);
        text("1st): Shad0w\n2nd): Blank\n3rd): Blank\n4th): Blank\n5th): Blank\n6th): Blank\n7th): Blank\n8th): Blank\n9th): Blank\n10th): Blank\n",40,150);
        mouseClicked = function() {scene=0;};
        noStroke();
        textAlign(CENTER,CENTER);
    };
    //}
    
    //Competitive vs Casual
    //{
    var timedOrCasual = function(){
        textSize(50);
        fill(255);
        push();
        stroke(0, 0, 0);
        strokeWeight(4);
        textSize(32);
        text("Selection Screen",200,40);
        fill(255, 255, 255);
        textSize(20);
        text("Level Select:", 200, 80);
        textSize(45);
        text("1", 200, 120);
        noStroke();
        triangle(160, 130, 175, 115, 175, 140);
        triangle(240, 130, 225, 115, 225, 140);
        stroke(0, 0, 0);
        strokeWeight(5);
        push();
        translate(80,195);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,190);
        rotate(angle);
        fill(50, pulse, 100);
        rect(-30,-30,60,60,10);
        pop();
        push();
        translate(80,345);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,340);
        rotate(angle);
        fill(50, pulse, 175);
        rect(-30,-30,60,60,10);
        pop();
        fill(0);
        rect(160,175,200,55,10);
        fill(5, 5, 5);
        rect(150,170,200,55,10);
        fill(255, 255, 255);
        textSize(30);
        text("Competitive",250,195);
        angle+=3;
        fill(0);
        rect(160,325,200,55,10);
        fill(4, 4, 4);
        rect(150,320,200,55,10);
        fill(255,255,255);
        textSize(50);
        text("Casual",250,345);
        textSize(20);
        text("(Normal, recommened for first play\n for Cube Effect and new CubeReduX\nplayers)",200,275);
        angle+=3;
        if(scene===5){
        if(mouseX>150&&mouseX<350&&mouseY>320&&mouseY<375){
            mouseClicked = function() {
                scene = 7;
            };
        }else{}
        if(mouseX>150&&mouseX<350&&mouseY>170&&mouseY<255){
            fill(0);
            rect(mouseX, mouseY, 200, 50);
            fill(200, pulse, pulse);
            rect(mouseX + 1, mouseY + 1, 198, 48);
            fill(255, 255, 255);
            textSize(15);
            text("Able to enter leaderboard,\nbased on deaths", mouseX + 100, mouseY + 25);
            mouseClicked = function() {
                scene = 7;
            };
        }else{}
        }
    };
    //}
    
    //Other Menu
    //{
    var otherMenu = function(){
        background(pulse, 150, 60);
        textSize(50);
        fill(255);
        push();
        stroke(0, 0, 0);
        strokeWeight(4);
        text("Other Menu",200,50);
        push();
        translate(80,142);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,140);
        rotate(angle);
        fill(50, pulse, 100);
        rect(-30,-30,60,60,10);
        pop();
        push();
        translate(80,242);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,240);
        rotate(angle);
        fill(50, pulse, 175);
        rect(-30,-30,60,60,10);
        pop();
        push();
        translate(340,332);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(330,330);
        rotate(angle);
        fill(100, pulse, pulse);
        rect(-30,-30,60,60,10);
        pop();
        fill(0);
        rect(160,116,200,55,10);
        fill(190);
        rect(150,110,200,55,10);
        fill(255, 255, 255);
        text("Credits",250,135);
        angle+=3;
        fill(0);
        rect(160,215,200,55,10);
        fill(190);
        rect(150,210,200,55,10);
        fill(255,255,255);
        push();
        textSize(40);
        pop();
        text("Settings",250, 235);
        textSize(50);
        fill(0);
        rect(50,306,200,55,10);
        fill(190);
        rect(40,300,200,55,10);
        fill(255, 255, 255);
        text("Back",140,325);
        angle+=3;
        fill(0,0,0,fade);
        rect(0,0,400,400);
        fade-=5;
        noStroke();
        
        if(scene===8){
        if(mouseX>150&&mouseX<350&&mouseY>110&&mouseY<165){
            mouseClicked = function () {
                scene=2;
            };
        }else{}
        if(mouseX>40&&mouseX<240&&mouseY>300&&mouseY<355){
            mouseClicked = function () {
                scene=0;
            };
        }else{;}
        }
        if(mouseX>150&&mouseX<350&&mouseY>210&&mouseY<265){
            fill(0);
            rect(mouseX, mouseY, 200, 50);
            fill(200, pulse, pulse);
            rect(mouseX + 1, mouseY + 1, 198, 48);
            fill(255, 255, 255);
            textSize(15);
            //text("Changes graphics to stuff\nlike Steve, however is slow", mouseX + 100, mouseY + 25);
            mouseClicked = function () {
                
            };
        }else{}
        noStroke();
    };
    //}
    
    //Titlescreen
    //{
    var title = function(){
        background(pulse, 150, 60);
        textSize(50);
        fill(255);
        push();
        stroke(0, 0, 0);
        strokeWeight(4);
        text("CubeReduX",200,50);
        push();
        translate(80,142);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,140);
        rotate(angle);
        fill(50, pulse, 100);
        rect(-30,-30,60,60,10);
        pop();
        push();
        translate(80,242);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(70,240);
        rotate(angle);
        fill(50, pulse, 175);
        rect(-30,-30,60,60,10);
        pop();
        push();
        translate(340,332);
        rotate(angle);
        fill(0);
        rect(-30,-30,60,60,10);
        pop(); 
        push();
        translate(330,330);
        rotate(angle);
        fill(100, pulse, pulse);
        rect(-30,-30,60,60,10);
        pop();
        fill(0);
        rect(160,116,200,55,10);
        fill(190);
        rect(150,110,200,55,10);
        fill(255, 255, 255);
        text("Play",250,132);
        angle+=3;
        fill(0);
        rect(160,215,200,55,10);
        fill(190)
        rect(150,210,200,55,10);
        fill(255,255,255);
        textSize(30);
        text("Leaderboard",250,235);
        textSize(50);
        fill(0);
        rect(50,306,200,55,10);
        fill(190);
        rect(40,300,200,55,10);
        fill(255, 255, 255);
        text("Other",140,325);
        angle+=3;
        fill(0,0,0,fade);
        rect(0,0,400,400);
        fade-=5;
        noStroke();
        
        //if(false){
        //    textSize(80);
        //    fill(255, 0, 0);
        //   text("CHEATER",200,200);
        //    play = false;
        //}else{play = true;}
        
        play = true;
        
        if(play&&scene===0){
        if(mouseX>150&&mouseX<350&&mouseY>110&&mouseY<165){
            mouseClicked = function () {
                scene=5;
            };
        }else{}
        if(mouseX>40&&mouseX<240&&mouseY>300&&mouseY<355){
            mouseClicked = function () {
                scene=8;
            };
        }else{}
        }
        if(mouseX>150&&mouseX<350&&mouseY>210&&mouseY<265){
            fill(0);
            rect(mouseX, mouseY, 200, 50);
            fill(200, pulse, pulse);
            rect(mouseX + 1, mouseY + 1, 198, 48);
            fill(255, 255, 255);
            textSize(15);
            text("Join by commenting,\nbased on deaths", mouseX + 100, mouseY + 25);
            mouseClicked = function () {
                scene=4;
            };
        }else{}
        noStroke();
    };
    //}
    
    //Credits
    //{
    var help = function(){
        background(50, pulse, pulse);
        fill(100);
        push();
        stroke(0, 0, 0);
        strokeWeight(5);
        rect(30,30,340,340,10);
        pop();
        fill(255, 255, 255);
        textSize(15);
        mouseClicked = function() {scene=8;};
        noStroke();
        text("Firstly, this is a platformer made\nwith Electric Dolphin's platformer\n template. This is a spinoff of\nEclipse's Cube Effect, on top of\nthat.\n\nFollowing that, most assets are from\nthe creators above, but I'm slowly\nchanging everything slowly, so if I haven't\nupdated this in a while, you know what's\neverything anyways.\n\nThe levels are developed by Noah, River,\nand myself. The old levels are made by\nElijah and Charlie, respectively.\n\n(Click anywhere to exit)",200,200);
    };
    //}
    
    //Intro
    //{
    var intro = function(){
        fill(0);
        textSize(35);
        fill(255);
        text("dynamicproductions",200,65);
        textSize(275);
        text("DP",200,210);
        textSize(20);
        text("roductions",325,325);
        rect(75,236,55,15);
        triangle(100, 210, 145, 245, 100, 245);
        triangle(100, 275, 145, 245, 100, 245);
        noStroke();
        fill(0,0,0,fade);
        rect(0,0,400,400);
        time++;
        if(time>100){fade+=5;}
        if(fade>300){scene=0;}
        
    };
    //}
    
    //Eclipse's Intro
    //{
    var eclipseIntro = function(){
        var eFade = 0;
        var eTime = 0;
        fill(0, 0, 0);
        stroke(255);
        strokeWeight(20);
        ellipse(200,220,200,200);
        textSize(50);
        fill(255);
        text("Eclipse Games",200,60);
        push();
        translate(200,220);
        rotate(270);
        arc(0, 0, 200, 200, 1, 180);
        pop();
        noStroke();
        fill(0,0,0,eFade);
        rect(0,0,400,400);
        while(scene===6) {eTime++;}
        if(eTime>100){eFade+=5;}
        if(eFade>300){scene=0;}
        
    };
    //}
    
    //Scences
    //{
    
    draw = function() {

    frameRate(60);
        
        if(scene===0){
            title();
        }
        
        if(scene===2){
            background(150,150,150);
            help();
        }
        if(scene===3){
            background(0);
            intro();
        }
        if(scene===4){
            background(50,pulse,pulse);
            leaderboard(0);
        }
        if(scene===5){
            background(50,pulse,pulse);
            timedOrCasual();
        }
        if(scene===6){
            background(0);
            eclipseIntro();
        }
        if(scene===7){
            background(0);
            var competitiveOrNah = true;
            scene=1;
        }
        if(scene===8){
            background(0);
            otherMenu();
        }
        pulse -= pulseS;
        if(pulse<0){pulseS = -1;}
        if(pulse>200){pulseS = 1;}
        
        //pulseP -= pulseSP;
        //if(pulseP<0){pulseS = -10;}
        //if(pulseP>200){pulseS = 10;}
        
        if(scene===1){
        background(255);
        textSize(20);
        
        if(initialize===true){
        death+=1; 
        if(win===true){death-=1;}
        if(win===false){}
        }
        background(255, 255, 255);
        yVel += 0.5;
        canJump = false;
        if (initialize) {
            enemies = [];
        }
    for (var i = 0; i < levels[level].length; i ++) {
        for (var j = 0; j < levels[level][i].length; j ++) {
            switch(levels[level][i][j]) {
                case playerL:
                    noStroke();
                    if (initialize) {
                        pX = j * 20;
                        pY = i * 20;
                    }
                break;
                
                case enemyL:
                    if (level === 4) {
                        fill(0, pulse, 255, 150);
                        rect(j * 20, i * 20, 20, 20);
                    }
                    if (initialize) {
                        enemies.push(new enemy(j * 20, i * 20));
                    }
                break;
                
                case blockL:
                    fill(0, 0, 0);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX + 20 > x && pX < x + 20 && pY + 20 > y && pY < y + 20) {
                        if (pX + 20 > x + round(xVel) + 1 && pX < x + round(xVel) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y && pY < y + 10) {
                            inWater = false;
                            yVel = 0;
                            pY = y - 20;
                            canJump = true;
                        }
                        if (pX + 20 > (x + round(xVel) + 1) && pX < (x + round(xVel)) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y + 20/2 && pY < (y + 20/2) + 20/2) {
                            yVel = 0.1;
                            pY = y + 20;
                        }
                        if (pX + 20 > x && pX < x + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x - 20;
                            xVel = 0;
                        }
                        if (pX + 20 > x + 10 && pX < x + 10 + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x + 20;
                            xVel = 0;
                        }
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case invisiBlockI:
                    var invisiBlock = 75;
                    fill(237, 228, 237, invisiBlock);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX + 20 > x && pX < x + 20 && pY + 20 > y && pY < y + 20) {
                        if (pX + 20 > x + round(xVel) + 1 && pX < x + round(xVel) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y && pY < y + 10) {
                            inWater = false;
                            yVel = 0;
                            pY = y - 20;
                            canJump = true;
                        }
                        if (pX + 20 > (x + round(xVel) + 1) && pX < (x + round(xVel)) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y + 20/2 && pY < (y + 20/2) + 20/2) {
                            yVel = 0.1;
                            pY = y + 20;
                        }
                        if (pX + 20 > x && pX < x + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x - 20;
                            xVel = 0;
                        }
                        if (pX + 20 > x + 10 && pX < x + 10 + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x + 20;
                            xVel = 0;
                        }
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case doorL:
                    fill(0, 0, 0);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX + 20 > x && pX < x + 20 && pY + 20 > y && pY < y + 20) {
                        if (pX + 20 > x + round(xVel) + 1 && pX < x + round(xVel) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y && pY < y + 10) {
                            inWater = false;
                            yVel = 0;
                            pY = y - 20;
                            canJump = true;
                        }
                        if (pX + 20 > (x + round(xVel) + 1) && pX < (x + round(xVel)) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y + 20/2 && pY < (y + 20/2) + 20/2) {
                            yVel = 0.1;
                            pY = y + 20;
                        }
                        if (pX + 20 > x && pX < x + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x - 20;
                            xVel = 0;
                        }
                        if (pX + 20 > x + 10 && pX < x + 10 + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                            pX = x + 20;
                            xVel = 0;
                        }
                    }
                break;
                    
                case fakeL:
                    fill(89, 89, 89);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case decoInvisL:
                    fill(237, 228, 237, invisiBlock - 25);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case decoRedL:
                    fill(255,0,0);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case decoBlueL:
                    fill(0, 0, 255);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case decoGreenL:
                    fill(0, 255, 0);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case wallL:
                    fill(227, 207, 93);
                    rect(j * 20, i * 20, 20, 20);
                break;
                
                case platformL:
                    fill(123, 123, 123);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case lavaL:
                    noStroke();
                    fill(255, pulse, 0);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        initialize = true;
                        win=false;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case spikeL:
                    fill(0, 0, 0);
                    rect(j * 20 + 8, i * 20, 4, 2);
                    rect(j * 20 + 6, i * 20 + 2, 2, 4);
                    rect(j * 20 + 4, i * 20 + 6, 2, 4);
                    rect(j * 20 + 2, i * 20 + 10, 2, 4);
                    rect(j * 20, i * 20 + 14, 2, 6);
                    rect(j * 20 + 12, i * 20 + 2, 2, 4);
                    rect(j * 20 + 14, i * 20 + 6, 2, 4);
                    rect(j * 20 + 16, i * 20 + 10, 2, 4);
                    rect(j * 20 + 18, i * 20 + 14, 2, 6);
                    fill(92, 92, 92);
                    rect(j * 20 + 8, i * 20 + 6, 6, 4);
                    rect(j * 20 + 6, i * 20 + 10, 10, 4);
                    rect(j * 20 + 4, i * 20 + 14, 14, 6);
                    fill(128, 125, 128);
                    rect(j * 20 + 8, i * 20 + 2, 4, 4);
                    rect(j * 20 + 6, i * 20 + 6, 3, 4);
                    rect(j * 20 + 4, i * 20 + 10, 3, 4);
                    rect(j * 20 + 2, i * 20 + 14, 3, 6);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        initialize = true;
                        win=false;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case waterSpikeL:
                    fill(0, pulse, 255, 150);
                    rect(j * 20, i * 20, 20, 20);
                    fill(0, 0, 0);
                    rect(j * 20 + 8, i * 20, 4, 2);
                    rect(j * 20 + 6, i * 20 + 2, 2, 4);
                    rect(j * 20 + 4, i * 20 + 6, 2, 4);
                    rect(j * 20 + 2, i * 20 + 10, 2, 4);
                    rect(j * 20, i * 20 + 14, 2, 6);
                    rect(j * 20 + 12, i * 20 + 2, 2, 4);
                    rect(j * 20 + 14, i * 20 + 6, 2, 4);
                    rect(j * 20 + 16, i * 20 + 10, 2, 4);
                    rect(j * 20 + 18, i * 20 + 14, 2, 6);
                    fill(92, 92, 92);
                    rect(j * 20 + 8, i * 20 + 6, 6, 4);
                    rect(j * 20 + 6, i * 20 + 10, 10, 4);
                    rect(j * 20 + 4, i * 20 + 14, 14, 6);
                    fill(128, 125, 128);
                    rect(j * 20 + 8, i * 20 + 2, 4, 4);
                    rect(j * 20 + 6, i * 20 + 6, 3, 4);
                    rect(j * 20 + 4, i * 20 + 10, 3, 4);
                    rect(j * 20 + 2, i * 20 + 14, 3, 6);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        initialize = true;
                        win=false;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case graySpikeL:
                    fill(89, 89, 89);
                    rect(j * 20, i * 20, 20, 20);
                    fill(0, 0, 0);
                    rect(j * 20 + 8, i * 20, 4, 2);
                    rect(j * 20 + 6, i * 20 + 2, 2, 4);
                    rect(j * 20 + 4, i * 20 + 6, 2, 4);
                    rect(j * 20 + 2, i * 20 + 10, 2, 4);
                    rect(j * 20, i * 20 + 14, 2, 6);
                    rect(j * 20 + 12, i * 20 + 2, 2, 4);
                    rect(j * 20 + 14, i * 20 + 6, 2, 4);
                    rect(j * 20 + 16, i * 20 + 10, 2, 4);
                    rect(j * 20 + 18, i * 20 + 14, 2, 6);
                    fill(92, 92, 92);
                    rect(j * 20 + 8, i * 20 + 6, 6, 4);
                    rect(j * 20 + 6, i * 20 + 10, 10, 4);
                    rect(j * 20 + 4, i * 20 + 14, 14, 6);
                    fill(128, 125, 128);
                    rect(j * 20 + 8, i * 20 + 2, 4, 4);
                    rect(j * 20 + 6, i * 20 + 6, 3, 4);
                    rect(j * 20 + 4, i * 20 + 10, 3, 4);
                    rect(j * 20 + 2, i * 20 + 14, 3, 6);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        initialize = true;
                        win=false;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case waterL:
                    //image(waterB, j * 20, i * 20, 20, 20);
                    fill(0, pulse, 255, 150);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        canJump = true;
                        inWater = true;
                        yVel = 0.5;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 20 && enemies[l].x < x + 20 && enemies[l].y > y - 20 && enemies[l].y < y + 20) {
                        enemies[l].yVel = 0.5;
                        }
                    }
                break;
                
                case jumpL:
                    fill(0, 21, 255);
                    rect(j * 20, i * 20, 20, 20);
                    fill(255, 213, 0);
                    rect(j * 20, i * 20, 20, 5);
                    rect(j * 20, i * 20+15, 20, 5);
                    //image(trampolineT, j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        yVel = -bounceHeight;
                        pY -= 1;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 20 && enemies[l].x < x + 20 && enemies[l].y > y - 20 && enemies[l].y < y + 20) {
                        enemies[l].yVel = -enemyBounceHeight;
                        enemies[l].y -= 1;
                    }
                    }
                break;
                
                case portalL:
                    fill(0, 255, 221);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        level ++;
                        win=true;
                        initialize = true;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case waterLavaT:
                    var waterLavaTimer = 130;
                    var waterLavaTimerR = 1;
                    waterLavaTimer += waterLavaTimerR;
                    if(waterLavaTimer<0){waterLavaTimerR = 1;}
                    if(waterLavaTimer>255){waterLavaTimerR = -1;}
                    fill(0, pulse, 255);
                    rect(j * 20, i * 20, 20, 20);
                    fill(255, pulse, 0, pulse);
                    rect(j * 20, i * 20, 20, 20);
                    x = j * 20;
                    y = i * 20;
                    if (waterLavaTimer > pulse) {
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        canJump = true;
                        inWater = true;
                        yVel = 0.5;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 20 && enemies[l].x < x + 20 && enemies[l].y > y - 20 && enemies[l].y < y + 20) {
                        enemies[l].yVel = 0.5;
                        }
                    }
                    } else {
                    if (pX > x - 20 && pX < x + 20 && pY > y - 20 && pY < y + 20) {
                        initialize = true;
                        win=false;
                        doInit = false;
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                    }
                break;
                
                case tp1L:
                    //image(portalP, j * 20, i * 20, 20, 20);
                    //fill(84, 58, pulse, pulse);
                    //rect(j * 20, i * 20, 20, 20);
                    fill(255, 200+pulse, pulse);
                    rect(j * 20, i * 20, 20, 20); 
                    x = j * 20;
                    y = i * 20;
                    if (pX + 20 > x && pX < x + 20 && pY + 20 > y && pY < y + 20) {
                        if (pX + 20 > x + round(xVel) + 1 && pX < x + round(xVel) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y && pY < y + 10) {
                            tp = true;
                        }
                        if (pX + 20 > (x + round(xVel) + 1) && pX < (x + round(xVel)) + 20 - (round(abs(xVel)) + 1) && pY + 20 > y + 20/2 && pY < (y + 20/2) + 20/2) {
                            tp = true;
                        }
                        if (pX + 20 > x && pX < x + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                           tp = true;
                        }
                        if (pX + 20 > x + 10 && pX < x + 10 + 10 && pY + 20 > y + round(yVel) + 1 && pY < y + round(yVel) + 1 + 20 - (round(abs(yVel)) + 1)) {
                           tp = true;
                        }
                    }
                    for (var l = 0; l < enemies.length; l ++) {
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y > y - 21 && enemies[l].y < y - 5) {
                        enemies[l].yVel = 0;
                        if (enemies[l].y > y - 20) {
                            enemies[l].y = y - 20;
                            enemies[l].yVel = 0;
                        }
                    }
                        if (enemies[l].x > x - 19 && enemies[l].x < x + 19 && enemies[l].y < y + 20 && enemies[l].y > y + 5) {
                        enemies[l].yVel = -enemies[l].yVel/4;
                        enemies[l].y += 2;
                    }
                        if (enemies[l].x > x - 20 && enemies[l].x < x - 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 0;
                    }
                        if (enemies[l].x < x + 20 && enemies[l].x > x + 9 && enemies[l].y > y - 17 && enemies[l].y < y + 17) {
                        enemies[l].dir = 1;
                    }
                    }
                break;
                
                case tp2L:
                    //image(portalP, j * 20, i * 20, 20, 20);
                    //fill(84, 58, pulse, pulse);
                    //rect(j * 20, i * 20, 20, 20);
                    fill(255, 200+pulse, pulse);
                    rect(j * 20, i * 20, 20, 20); 
                    if(tp){
                        pX = j * 20;
                        pY = i * 20;
                        tp=false;
                    }
            }
        }
    }
    if (initialize) {
        xVel = 0;
        yVel = 0;
    }
    if (doInit) {
    initialize = false;
    }
    doInit = true;
    player();
    pY += yVel;
    pX += xVel;
    if (keys[39]) {
        if (xVel < -3) {
            xVel = -3;
        }
        if (xVel < maxSpeed) {
        xVel += acceleration;
        } else {
            xVel = maxSpeed;
        }
    
    } else if (xVel > deceleration) {
        xVel  -= deceleration;
    
    } else if (keys[37]) {
        if (xVel > 3) {
            xVel = 3;
        }
        if (xVel > -maxSpeed) {
        xVel -= acceleration;
        } else {
            xVel = -maxSpeed;
        }
        if (touchingBlock) {
            pX -= 0.1;
    }
    
    } else if (xVel < -deceleration) {
        xVel += deceleration;
    
    } else {
        xVel = 0;
    }
    
    if (keys[38] && canJump) {
        yVel = -jumpHeight;
        canJump = false;
        pY -= gravity;
        if(inWater===false){
        }
    }
    
    for (var i = 0; i < enemies.length; i ++) {
        enemies[i].draw();
        if (pX > enemies[i].x - 20 && pX < enemies[i].x + 20 && pY > enemies[i].y - 20 && pY < enemies[i].y + 20) {
            initialize = true;
            win=false;
        }
        if (enemies[i].x < 1) {
            enemies[i].dir = 1;
        }
        if (enemies[i].x > width - 21) {
            enemies[i].dir = 0;
        }
    }
    if (pY > height-20){pY=height-20;}
    if (pX > width-20){pX=width-20;xVel=0;}
    if (pX < 0){pX=0;xVel=0;}
    if (levelText[level]) {
        fill(0, 0, 0);
        text(levelText[level][0], levelText[level][1], levelText[level][2]);
    }
    }
    
    };
    //}
    