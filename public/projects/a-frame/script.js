var cameraPosition;
var ballPosition;
var buttonPressed = false;
var winText = "Level ";
var winText2 = " completed!";
var level = 1;
var levelActual = 1;
var timeTick = 0;
var reloadTick = 0;
var goalScored = false;
var stopWatch = "";
var seconds = 0;
var minutes = 0;
var miliseconds = 0;
var enemyTeam = document.getElementById("enemyTeam");
var playerLoaded = false;
var playerSpeed = 2;
var player1 = "player1";
var sound = new Howl({
    src: ['goal.mp3'],
    autoplay: true,
    loop: true,
    volume: 0.1,
});
var times = "";
var siu = new Howl({
    src: ['siu.mp3'],
});
var kick = new Howl({
    src: ['kick.mp3'],
    volume: 0.1,
});
var timeActual = "";
var hasGameStarted = false;
var button = document.getElementById("buttonDiv");
var text = document.getElementById("playbutton");
var textGone = false;
var buttonGone = false;
var highScore = localStorage.getItem("level");
var lastGame = localStorage.getItem("lastGame");
var stringer = "";
AFRAME.registerComponent('rotation-reader', {
    tick: function () {
        // `position` is a three.js Vector3.
        cameraPosition = this.el.object3D.position;
        legTranslation();
    }
});
AFRAME.registerComponent('ball', {
    tick: function () {
        // `position` is a three.js Vector3.
        
        ballPosition = this.el.object3D.position;
        if (!hasGameStarted && level === 1) {
            textText.setAttribute('text', 'value', "Prison FC - Beat this ragtag team of\nprisoners and score goals!");
        } else if (level === 1) {
            textText.setAttribute('text', 'value', winText + level + " - Dribble and shoot into the opposing goal!");

        } else {
            textText.setAttribute('text', 'value', winText + level);
        }
        textBox.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y + 0.6, z:cameraPosition.z});
        textBox.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
        scoreText.setAttribute('text', 'value', timeActual);
        score.setAttribute('position', {x:cameraPosition.x + 0.5, y:cameraPosition.y + 0.6, z:cameraPosition.z});
        score.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
        playbutton.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
        
        if (!goalScored) {
            goalCheck();
        }
        if (goalScored) {
            if (levelActual = level) {
                levelActual += 1;
            }
            if (highScore < levelActual) {
                localStorage.setItem("level", levelActual);
            }
            timeTick += 1;
            textText.setAttribute('text', 'value', winText + level + winText2 + "\nPrisoners will now become faster!");
            textBox.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y + 0.6, z:cameraPosition.z});
            textBox.object3D.rotation.set(
                THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
                THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
                THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
            );

            if (timeTick >= 500) {
                goalLogic();
            }
        }
        /*var playerEl = document.querySelector('[ball]');
            playerEl.addEventListener('collide', function (e) {
            console.log('Player has collided with body #' + e.detail.body.id);

            if ((e.detail.body.id = 23)) {

            }

            if ((e.detail.body.id = 21) || (e.detail.body.id = 22)|| (e.detail.body.id = 20)|| (e.detail.body.id = 19)|| (e.detail.body.id = 18)|| (e.detail.body.id = 17)|| (e.detail.body.id = 16)|| (e.detail.body.id = 15)) {

            }
        }); */
    }
});
AFRAME.registerComponent('button', {
    tick: function () {
        
        if (!hasGameStarted) {
            if (lastGame === "null") {
                timeActual = "Last Game:\nNo History Yet!";
            } else {
                timeActual = "Last Game:\n" + lastGame;
            }
        }
        if (!buttonPressed) {
            buttonDiv.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y, z:cameraPosition.z});
            playbutton.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y, z:cameraPosition.z});
        }
        buttonDiv.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
        
        this.el.addEventListener('click', function (evt) {
            buttonPressed = true;
            buttonDiv.setAttribute('position', {x:cameraPosition.x + 100, y:cameraPosition.y, z:cameraPosition.z});
            playbutton.setAttribute('position', {x:cameraPosition.x + 100, y:cameraPosition.y, z:cameraPosition.z});
            camera.setAttribute('position', {x:0, y:1.6, z:25});
            balls.setAttribute('position', {x:0, y:0.25, z:23});
            timeActual = "";
            hasGameStarted = true;
            if (lastGame === "null") {
                lastGame = "";
            } 
            if (highScore === "null") {
                highScore = 0;
            } 
        });
    }
});
AFRAME.registerComponent('move-towards', {
    schema: {
        targetId: { type: 'string' },  // ID of the target entity to move towards
        speed: { default: 0.1 }        // Speed of movement
    },

    tick: function () {
        // Ensure target entity is defined
        var targetEntity = document.getElementById(this.data.targetId);
        var speedScale = this.data.speed * level * 2;
        if (!targetEntity) return;

        // Get current positions
        var currentPosition = this.el.object3D.position.clone();
        var targetPosition = targetEntity.object3D.position.clone();

        // Calculate direction vector from currentPosition to targetPositions
        var direction = targetPosition.clone().sub(currentPosition).normalize();
        direction.setY(0);

        if (hasGameStarted) {

            // Update position based on direction and speed (move towards)
            this.el.object3D.position.add(direction.multiplyScalar(speedScale));

            // Calculate rotation to face the target
            var angle = Math.atan2(direction.x, direction.z);
            this.el.object3D.rotation.set(0, angle, 0);
        }
    }
});
AFRAME.registerComponent('levelText', {
    tick: function () {
        // `position` is a three.js Vector3.
        textText.setAttribute('text', 'value', winText + level);
        textBox.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y + 0.6, z:cameraPosition.z});
        textBox.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
    }
});
function legTranslation() {
    legs.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y, z:cameraPosition.z +	 0.1});
    legs.object3D.rotation.set(
        THREE.MathUtils.degToRad(0),
        THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
        THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
    );
}

function reloadFunc() {
    //sound.mute = true;
    reloadTick++;
    if (reloadTick >= 500) {
        window.location.reload();
    }
}
function goalCheck() {
    if (hasGameStarted) {
        if (((ballPosition.z >= 27.5) && (ballPosition.x >= 1.25)) || ((ballPosition.z >= 27.5) && (ballPosition.x <= -1.25))) {
            textText.setAttribute('text', 'value', 'Ball out of play! Restarting...');
            reloadFunc();
        } else if ((ballPosition.z >= 27.5)) {
            textText.setAttribute('text', 'value', 'Own goal! Restarting...');
            reloadFunc();
            }
        
        if (((ballPosition.z <= 0) && (ballPosition.x <= -1.25)) || ((ballPosition.z <= 0) && (ballPosition.x >= 1.25))) {
            textText.setAttribute('text', 'value', 'Ball out of play! Restarting...');
            reloadFunc();
        } else if ((ballPosition.z <= 0)) {
            siu.play();
            goalScored = true;
        }
        if (ballPosition.x <= -11.525 || ballPosition.x >= 11.525) {
            textText.setAttribute('text', 'value', 'Ball out of play! Restarting...');
            reloadFunc();
        }
    }
}

function goalLogic () {	
    timeTick = 0;
    if (level === 1) {
        timeActual = "Level " + level + " " + minutes + ":" + seconds + ":" + miliseconds;
    } else if (level > 1) {
        times = "Level " + level + " " + minutes + ":" + seconds + ":" + miliseconds;
        timeActual = timeActual + "\n" + times;
        stringer = timeActual
        localStorage.setItem("lastGame", stringer);
    }
    goalScored = false;
    console.log("TELEPORT HAPPENED");
    camera.setAttribute('position', {x:0, y:1.6, z:25});
    goalie1.setAttribute('position', {x:0, y:1, z:3});
    goalie2.setAttribute('position', {x:0, y:1, z:8});
    goalie3.setAttribute('position', {x:6, y:1, z:10});
    goalie4.setAttribute('position', {x:-6, y:1, z:10});
    goalie5.setAttribute('position', {x:-3, y:1, z:13});
    goalie6.setAttribute('position', {x:3, y:1, z:13});
    goalie7.setAttribute('position', {x:-6, y:1, z:13});
    goalie8.setAttribute('position', {x:6, y:1, z:17});
    goalie9.setAttribute('position', {x:0, y:1, z:20});
    balls.pause();
    balls.setAttribute('position', {x:0, y:0.25, z:23});
    balls.play();
    textText.setAttribute('text', 'value', '');
    textBox.object3D.rotation.set(
        THREE.MathUtils.degToRad(0),
        THREE.MathUtils.degToRad(0),
        THREE.MathUtils.degToRad(0)
    );
    level = levelActual;
}
AFRAME.registerComponent('timer', {
    tick: function () {
        if (hasGameStarted) {
            timeElapsed.setAttribute('text', 'value', stopWatch);
        } else if (highScore === "null") {
            timeElapsed.setAttribute('text', 'value', "High Score: No High Score Yet!");
        } else {
            timeElapsed.setAttribute('text', 'value', "High Score: Level " + highScore);
        }
        time.setAttribute('position', {x:cameraPosition.x, y:cameraPosition.y + 0.5, z:cameraPosition.z});
        time.object3D.rotation.set(
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').x),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').y),
            THREE.MathUtils.degToRad(camera.getAttribute('rotation').z)
        );
        if (hasGameStarted) {
            stopWatch = minutes + ":" + seconds + ":" + miliseconds;
            miliseconds ++;
            if (miliseconds == 100) {
                seconds ++;
                miliseconds = 0;
            }
            if (seconds == 60) {
                minutes ++;
                seconds = 0;
            }
        }
    }
});	