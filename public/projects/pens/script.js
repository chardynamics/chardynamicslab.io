disableFriendlyErrors = true;


var currentPrompt = ""; // Stores the current text being displayed
var promptIndex = 0; // Index of the current character to be added
var typingSpeed = 50; // Adjust the typing speed (milliseconds per character)
var typingTimer; // Timer to control the typing effect

var aWindowWidth = 1366;

var preAWindowWidth;
var preWindowHeight;
var stage = 2;
var player1;

var paused = false;

var introVar = {
	cubeRotate: 0,
	tankY: 300,
	tankRotate: 0,
	turretRotate: 0,
	bulletX: 454,
	textCover: 0,
	bulletTransparency: 0,
	soundTransparency: 0,
}

var pulse = {
	var: 200,
	rate: 5,
}

var fade = {
	intro: 255,
	out: 0
}
var keys = [];

function keyPressed() {
	keys[keyCode] = true;
}

function keyReleased() {
	keys[keyCode] = false;
}


function setup() {
	//var canvas = createCanvas(1366, 768);
	createCanvas(windowWidth, windowHeight);
	windowResized();
	var canvas = document.querySelector("canvas");
	canvas.style.margin = 'auto';
	document.getElementById("script-holder").appendChild(canvas);
	rectMode(CENTER);
	textAlign(CENTER, CENTER);
	angleMode(DEGREES);
	textStyle(BOLD);
	noStroke();
	player1 = new Sprite();

	//player1.draw = () => {
		let ballArt = `
			...bwwwwb
			.bbbbbbbbbb
			.bbbwwwwbbb
			wbwwbbbbwwbw
			.uubbbbuu
			.uuuuuu
			..uuuuuu`;
	
		let palette = {
			w: color(255, 255, 255),
			b: color(0)
		};
		player1.img = spriteArt(ballArt, 16, palette);
	//}
}

function pulseMath() {
	pulse.var -= pulse.rate;
	if(pulse.var<125){pulse.rate = -1;}
	if(pulse.var>225){pulse.rate = 1;}
}

function intro() {
	introVar.cubeRotate += 5;
	
	if (introVar.tankY > 195) {
		introVar.tankY -= 1;
	}
	if ((introVar.tankY == 195) && (introVar.tankRotate < 25)) {
		introVar.tankRotate += 1;
		introVar.turretRotate = introVar.tankRotate;
	}
	if (introVar.tankRotate == 25 && introVar.turretRotate < 90) {
		introVar.turretRotate += 1;
	}
	if (introVar.turretRotate == 90) {
		if (introVar.bulletX <= 1525) {
			introVar.bulletX += 8;
		}
		if ((introVar.bulletX >= 497)) {
			introVar.bulletTransparency = 255;
			introVar.soundTransparency = 50;
		}
	}
	if ((introVar.bulletX >= 536)) {
		introVar.textCover += 8;
	}
	if ((introVar.bulletX >= 1525) && (fade.out < 255)) {
		fade.out += 2.5;
	}
	
	background(0);
	push();
	fill(255, 255, 255);
	textSize(800); //I'm just using this as a general scale/ratio factor, although it only works with appropriate ratios
	text("DP", 675, 350.5);
	textSize(75);
	text("roductions", 1070, 550.5);
	push();
	translate(150, 675);
	rotate(introVar.cubeRotate);
	fill(-pulse.var, pulse.var, pulse.var + 100);
	rect(0, 0, 125, 125, 15);
	pop();
	textSize(75);
	text("X", 150, 680);
	push();
	translate(110, introVar.tankY);
	fill(50, 0, 0);
	rect(-12,0,5,35,5);
	rect(12,0,5,35,5);
	fill(0, 120, 0);
	rect(0,0,20,40,5);
	pop();
	textSize(125);
	text("...and more", 900, 700);
	rectMode(CORNER);
	fill(0);
	//rect(1445, 670, bullet.textCover, 122);
	rect(575, 625, introVar.textCover, 122);
	push();
	translate(introVar.bulletX, 663.5);
	fill(100, 100, 100, introVar.soundTransparency);
	triangle(-7.5, 45, 40, 17.5, -7.5, -9);
	fill(158, 60, 14, introVar.bulletTransparency);
	triangle(2, 27.5, 45, 17.5, 2, 10);
	pop();
	rectMode(CENTER);
	push();
	translate(110, introVar.tankY);
	rotate(introVar.turretRotate);
	fill(0, 100, 0);
	rect(0,0,15,15,5);
	rect(0,-20,5,25,0);
	pop();
	pop();
	
	if (fade.intro > 0) {
		fill(0, 0, 0, fade.intro);
		rect(aWindowWidth/2, windowHeight/2, aWindowWidth, windowHeight);
		fade.intro -= 2.5;
	}

	fill(0, 0, 0, fade.out);
	rect(aWindowWidth/2, windowHeight/2, aWindowWidth, windowHeight);
	
	if (fade.out >= 255) {
		fade.intro = 255;
		stage = 2;
	}
}

function level1() {
	background(255);

}

var prompts = [
	"\"...Welcome, Muzzl.\nAgent, you might have forgotten how to operate yourself so let's get you rehabilitated.\nMove around in all axes, move the turret, and fire.\nWhen you're ready, step on the pressure plate ahead to begin.\""
];

function debug() {
	fill(255, 0, 0);
	textSize(25);
	text(windowHeight/2, mouseX + 40, mouseY + 5)
	text(100, mouseX + 40, mouseY + 35)
}

function windowResized() {
	aWindowWidth = Math.floor(windowHeight * (16/9));
	preAWindowWidth = aWindowWidth;
	preWindowHeight = windowHeight;
	commsHeight = windowHeight/2 + ((windowHeight/2)/2);
	commsWidth = aWindowWidth/2;
	resizeCanvas(aWindowWidth, windowHeight);
}

function draw() {
	if (stage == 1) {
		intro();
	} else if (stage == 2) {
		level1();
	} else if (stage == 3) {
		level2();
	}
	debug();
}  